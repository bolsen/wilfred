%%%-------------------------------------------------------------------
%%% @author Brian Olsen <brian@bolsen.org>
%%% @copyright (C) 2011, Brian Olsen
%%% @doc
%%% 
%%% @end
%%% Created : 12 Apr 2011 by Brian Olsen <brian@bolsen.org>
%%%-------------------------------------------------------------------
-module(campfire_event).

-export([start_link/1,
		 add_handler/2,
		 delete_handler/2,
		 new_message/1]).

-define(SERVER, ?MODULE).

start_link(RoomId) ->
	Ok = gen_event:start_link({local, ?SERVER}),
	Ok.
	
add_handler(Handler, Args) ->
	gen_event:add_handler(?SERVER, Handler, Args).

delete_handler(Handler, Args) ->
	gen_event:delete_handler(?SERVER, Handler, Args).

new_message(Message) ->
	gen_event:notify(?SERVER, {message, Message}).
