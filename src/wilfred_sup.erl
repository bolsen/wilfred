-module(wilfred_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), {I, {I, start_link, []}, permanent, 5000, Type, [I]}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
	ibrowse:start(),
	ssl:start(),

	{ok, RoomId} = application:get_env(wilfred, room),
	ChatStream = {campfire_stream, {campfire_stream, start_link, [RoomId]},
				  permanent, 2000, worker, [campfire_stream]},
	EventManager = {campfire_event, {campfire_event, start_link, [RoomId]},
					permanent, 2000, worker, [campfire_event]},
	RestartStrategy = {one_for_one, 10, 3600},
    {ok, { RestartStrategy, [EventManager, ChatStream]}}.

