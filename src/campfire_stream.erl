%%%-------------------------------------------------------------------
%%% @author Brian Olsen <brian@bolsen.org>
%%% @copyright (C) 2011, Brian Olsen
%%% @doc
%%%
%%% Manages a stream of data
%%%
%%% @end
%%% Created : 12 Apr 2011 by Brian Olsen <brian@bolsen.org>
%%%-------------------------------------------------------------------
-module(campfire_stream).

-behaviour(gen_server).

%% API
-export([start_link/1, current_messages/0, delete_all_messages/0]).

%% gen_server callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
		 terminate/2, code_change/3]).

-define(SERVER, ?MODULE). 

-record(state, {room_id, req_id, messages = []}).

%%%===================================================================
%%% API
%%%===================================================================



%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @spec start_link() -> {ok, Pid} | ignore | {error, Error}
%% @end
%%--------------------------------------------------------------------
start_link(RoomId) ->
	gen_server:start_link({local, ?SERVER}, ?MODULE, [RoomId], []).

start_chat_connection(RoomId) ->
	campfire:stream_chat(RoomId, self()).

current_messages() ->
	gen_server:call(?MODULE, current_messages).

delete_all_messages() ->
	gen_server:call(?MODULE, delete_all_messages).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([RoomId]) ->
	campfire:room(join, RoomId),
	{ok, Handlers} = application:get_env(wilfred, handlers),
	lists:foreach(fun(H) -> add_handler(H, [RoomId]) end, Handlers),
	{ibrowse_req_id, ReqId} = start_chat_connection(RoomId),
	{ok, #state{room_id = RoomId, req_id = ReqId}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @spec handle_call(Request, From, State) ->
%%                                   {reply, Reply, State} |
%%                                   {reply, Reply, State, Timeout} |
%%                                   {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, Reply, State} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_call(current_messages, _From, State) ->
	Messages = State#state.messages,
	{reply, Messages, State#state{messages = []}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @spec handle_cast(Msg, State) -> {noreply, State} |
%%                                  {noreply, State, Timeout} |
%%                                  {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_cast(delete_all_messages, State) ->
	{noreply, State#state{messages = []}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info({ibrowse_async_headers, ReqId, _Status, _Headers}, State) ->
	ibrowse:stream_next(ReqId),
	{noreply, State};

handle_info({ibrowse_async_headers, ReqId, Body}, State) ->
	ibrowse:stream_next(ReqId),
	{noreply, State#state{messages = [Body | State#state.messages]}};

handle_info({ibrowse_async_response,OldReqId,{error,req_timedout}}, State) ->
	{ibrowse_req_id, ReqId} = start_chat_connection(State#state.room_id),
	{noreply, State#state{req_id = ReqId}};

handle_info({ibrowse_async_response,ReqId, Data}, State) ->
	ibrowse:stream_next(ReqId),
	case Data of
		" " ->
			{noreply, State};
		_ ->
			campfire_event:new_message(json:decode_string(Data)),
			{noreply, State#state{messages = [json:decode_string(Data) | State#state.messages]}}
	end;

handle_info({error,{conn_failed,{error,_}}}, State) ->
	campfire:stream_chat(State#state.room_id, self()),
	error_logger:info("Lost connection, waiting 5 seconds ...~n"),
	timer:sleep(5000),
	{noreply, State}.




%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do 	any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, State) ->
	campfire:room(leave, State#state.room_id),
	ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
