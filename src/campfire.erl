-module(campfire).

-export([me/0, request/2, request/3, rooms/0, rooms/1, room/2, room/3, stream_chat/2, campfire_domain/0]).

me() ->
 	campfire:request(get, "/users/me.json").

rooms() ->
 	campfire:request(get, "/rooms.json").

rooms(present) ->
	campfire:request(get, "/presence.json").

room(info, RoomId) ->
 	campfire:request(get, room_url(RoomId));

room(join, RoomId) ->
 	campfire:request(post, room_url(RoomId, "join"));

room(leave, RoomId) ->
	campfire:request(post, room_url(RoomId, "leave")).

%% room(listen) ->
%% 	ok;

room(speak, RoomId, Text) ->
	Json = json:encode({struct, [{message, {struct, [{body, Text}]}}]}),
	campfire:request(post, room_url(RoomId, "speak"), lists:flatten(Json)).

%% room(paste, RoomId, Text) ->
%% 	ok.


%% room(sound) ->
%% 	ok;

%% room(lock) ->
%% 	ok;a

%% room(unlock) ->
%% 	ok.


stream_chat(RoomId, StreamToPid) ->
	ibrowse:send_req("https://streaming.campfirenow.com" ++ room_url(RoomId, "live"), [{"Content-Type", "application/json"}], get, [], [{basic_auth, {auth_token(), "x"}}, {stream_to, {StreamToPid, once}}]).

room_url(RoomId) ->
	"/room/" ++ RoomId ++ ".json".

room_url(RoomId, Type) ->
	"/room/" ++ RoomId ++ "/" ++ Type ++ ".json".

request(Method, Path) ->
	request(Method, Path, []).

request(Method, Path, Body) ->
	{ok, Status, _Headers, ResponseBody} = ibrowse:send_req(campfire_domain() ++ Path, [{"Content-Type", "application/json"}], Method, Body, [{basic_auth, {auth_token(), "x"}}]),
	case Status of
		[$2 | _ ]  ->
			json:decode_string(ResponseBody);
		_Error ->
			% Throw an error
			{error, Status, json:decode_string(ResponseBody)}
	end.

%% Configurations handled by the wilfred application.

campfire_domain() ->
	{ok, Domain} = application:get_env(wilfred, domain),
	Domain.
	
auth_token() ->
	{ok, Token} = application:get_env(wilfred, token),
	Token.
